<?php

use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\Storage;

function uploadFileLocal($path, $base64)
{
    Storage::disk('local')->put($path, base64_decode($base64));

    return "storage/$path";
}

function generateTokenJwt(User $user)
{
    $key = config('app.key');
    $token = array(
        "iss" => "https://helesmedianetwork.com",
        "aud" => "https://helesmedianetwork.com",
        "iat" => 1356999524,
        "nbf" => 1357000000,
        "user" => $user,
    );
    return JWT::encode($token, $key, 'HS256');
}

function decodeTokenJwt($token)
{
    $key = config('app.key');
    return JWT::decode($token, $key, array('HS256'));
}
