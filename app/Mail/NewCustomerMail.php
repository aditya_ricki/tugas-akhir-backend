<?php

namespace App\Mail;

use App\Models\PacketUser;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewCustomerMail extends Mailable
{
    use Queueable, SerializesModels;

    public $packetUser;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(PacketUser $packetUser)
    {
        $this->packetUser = $packetUser;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.new-customer')
            ->from(env("MAIL_USERNAME"), env("APP_NAME"))
            ->subject("New Customer");
    }
}
