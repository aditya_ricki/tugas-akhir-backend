<?php

namespace App\Jobs;

use App\Mail\ScheduleDoneMail;
use App\Models\Schedule;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ScheduleDoneJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $schedule, $finances;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Schedule $schedule, Collection $finances)
    {
        $this->schedule = $schedule;
        $this->finances = $finances;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->finances as $finance) {
            Mail::to($finance)->send(new ScheduleDoneMail($this->schedule));
        }

        Mail::to($this->schedule->bill->packetUser->user->email)->send(new ScheduleDoneMail($this->schedule));
    }
}
