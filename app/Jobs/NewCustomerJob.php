<?php

namespace App\Jobs;

use App\Mail\NewCustomerMail;
use App\Models\PacketUser;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Database\Eloquent\Collection;

class NewCustomerJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $packetUser, $techs;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PacketUser $packetUser, Collection $techs)
    {
        $this->packetUser = $packetUser;
        $this->techs = $techs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->techs as $tech) {
            Mail::to($tech)->send(new NewCustomerMail($this->packetUser));
        }
    }
}
