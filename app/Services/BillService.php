<?php

namespace App\Services;

use App\Jobs\NewBillJob;
use App\Jobs\SuccessPaymentJob;
use Throwable;
use Midtrans\Snap;
use Midtrans\Config;
use Midtrans\Transaction;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Bill\EloquentRepository;
use App\Repositories\PacketUser\EloquentRepository as PacketUserEloquentRepository;
use App\Repositories\User\EloquentRepository as UserEloquentRepository;

class BillService
{
    protected $repository, $packetUserRepository, $userRepository;

    public function __construct(EloquentRepository $repository, PacketUserEloquentRepository $packetUserRepository, UserEloquentRepository $userRepository)
    {
        Config::$serverKey = env('MIDTRANS_SERVER_KEY');
        Config::$isProduction = false;
        Config::$isSanitized = true;
        Config::$is3ds = true;
        $this->repository = $repository;
        $this->packetUserRepository = $packetUserRepository;
        $this->userRepository = $userRepository;
    }

    public function readAllPaginatedByPacketUser($request, $packetUserId)
    {
        try {
            $packetUser = $this->packetUserRepository->readOne($packetUserId);

            if (!$packetUser) {
                throw new InvalidArgumentException("Packet user not found");
            }

            return $this->repository->readAllPaginatedByPacketUser($request, $packetUser);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }
    }

    public function readAll($request)
    {
        try {
            return $this->repository->readAll($request);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to read all data');
        }
    }

    public function readAllPaginated($request)
    {
        try {
            return $this->repository->readAllPaginated($request);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to read all paginated data');
        }
    }

    public function readOne($billId)
    {
        try {
            return $this->repository->readOne($billId);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to read one data');
        }
    }

    public function payment($request, $billId)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "email" => "required",
            "phone" => "required",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $bill = $this->repository->readOne($billId);

            if (!$bill) {
                throw new InvalidArgumentException("Bill not found");
            }

            $paymentUrl = Snap::createTransaction([
                "transaction_details" => [
                    "order_id" => $bill->transaction_code,
                    "gross_amount" => $bill->price,
                ],
                "customer_details" => [
                    "first_name" => $request->name,
                    "last_name" => "",
                    "email" => $request->email,
                    "phone" => $request->phone,
                ]
            ])->redirect_url;

            $this->repository->payment($paymentUrl, $bill);
        } catch (Throwable $th) {
            DB::rollback();
            Log::info($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }

        DB::commit();
        return $paymentUrl;
    }

    public function getStatusPayment($billId)
    {
        try {
            $bill = $this->repository->readOne($billId);

            if (!$bill) {
                return new InvalidArgumentException("Bill not found");
            }

            if ($bill->status > 1 && $bill->method == "PAYMENT_GATEWAY") {
                $statusTransaction = Transaction::status($bill->transaction_code);
            }
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to get status payment');
        }

        return [
            'bill' => $bill,
            'status_transaction' => $statusTransaction ?? null,
        ];
    }

    public function manualPayment($billId)
    {
        try {
            $bill = $this->repository->readOne($billId);

            if (!$bill) {
                return new InvalidArgumentException("Bill not found");
            }

            if ($bill->payment_url && !in_array($bill->status, [2, 4])) {
                Transaction::cancel($bill->transaction_code);
            }
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to create manual payment');
        } finally {
            DB::beginTransaction();

            try {
                if ($bill->status != 2) {
                    $this->repository->manualPayment($bill);
                }

                // notif
                dispatch(new SuccessPaymentJob($bill, $this->userRepository->readAllByRole(["F"])));
            } catch (Throwable $th) {
                DB::rollback();
                Log::info($th->getMessage());
                throw new InvalidArgumentException($th->getMessage());
            }
        }

        DB::commit();
        return true;
    }

    public function generateMonthlyBill($request)
    {
        DB::beginTransaction();
        try {
            // check if bill where type MONTHLY already exist
            $billThisMonth = $this->repository->getMonthlyBill(date('Y-m'));

            if (count($billThisMonth)) {
                throw new InvalidArgumentException("Bill for the month of " . date('F') . " already generated");
            }

            // if not, get all packet user
            $packetUsers = $this->packetUserRepository->readAll($request);

            // create new bill for all packet user
            foreach ($packetUsers as $packetUser) {
                $request->description = "Bill for the month of " . date('F');
                $request->price = $packetUser->price;
                $request->type = "MONTHLY";

                $bill = $this->repository->create($request, $packetUser);

                // notify
                dispatch(new NewBillJob($bill));
            }
        } catch (Throwable $th) {
            DB::rollback();
            Log::info($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }

        DB::commit();
        return true;
    }
}
