<?php

namespace App\Services;

use Throwable;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Repositories\User\EloquentRepository;

class ProfileService
{
    protected $repository;

    public function __construct(EloquentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getProfile($request)
    {
        try {
            $profile = $this->repository->readOne(decodeTokenJwt($request->header('Authorization'))->user->id);
            return $profile;
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException('Error get profile');
        }
    }

    public function updateProfile($request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "phone" => "required",
            "whatsapp" => "required",
            "address" => "required",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $data = $this->repository->readOne(decodeTokenJwt($request->header('Authorization'))->user->id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $fileName = str_shuffle("QWERTYUIOPASDFGHJKLZXCVBNM") . ".png";

            if ($request->photo) {
                $request->path_photo = uploadFileLocal("users/photo/$fileName", $request->photo);
            }

            if ($request->id_card) {
                $request->path_id_card = uploadFileLocal("users/id_card/$fileName", $request->id_card);
            }

            $update = $this->repository->update($request, $data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to update data");
        }

        DB::commit();
        return $update;
    }

    public function updatePassword($request)
    {
        $validator = Validator::make($request->all(), [
            "password" => "required|string|min:8",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $data = $this->repository->readOne(decodeTokenJwt($request->header('Authorization'))->user->id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $update = $this->repository->updatePassword($request, $data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to update data");
        }

        DB::commit();
        return $update;
    }
}
