<?php

namespace App\Services;

use Throwable;
use Illuminate\Http\Request;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Validator;
use App\Repositories\User\EloquentRepository;

class SocialService
{
    protected $request, $repository;

    public function __construct(Request $request, EloquentRepository $repository)
    {
        $this->request = $request;
        $this->repository = $repository;
    }

    public function getTargetUrl($provider)
    {
        try {
            return Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }
    }

    public function checkUser($provider)
    {
        DB::beginTransaction();

        try {
            $payload = Socialite::driver($provider)->stateless()->user();
            $email = $payload->user['email'] ?? '';
            $name = $payload->user['name'] ?? '';
            $id = $payload->user['id'] ?? '';

            if (!$email) {
                throw new InvalidArgumentException('Email not found');
            }

            $user = $this->repository->readOneByEmail($email);

            if (!$user) {
                $this->request->request->add([
                    'name' => $name,
                    'email' => $email,
                    'password' => str_shuffle("1234567890"),
                    'role' => 'C',
                    'phone' => '',
                    'whatsapp' => '',
                    'address' => '',
                    'photo' => '',
                    'id_card' => '',
                    'otp' => '',
                    'email_verified_at' => date('Y-m-d H:i:s'),
                ]);

                $user = $this->repository->create($this->request);
            }

            $this->repository->updateRandomKey($user);

            $token = generateTokenJwt($user);
        } catch (Throwable $th) {
            DB::rollback();
            Log::info($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }

        DB::commit();
        return $token;
    }
}
