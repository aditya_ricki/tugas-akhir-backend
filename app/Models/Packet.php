<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Packet extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $guarded = [];
    protected $appends = [
        'image_formatted',
        'price_formatted',
    ];

    public function getImageFormattedAttribute()
    {
        $image = '';

        if ($this->attributes['image']) {
            $image = env('APP_URL') . $this->attributes['image'];
        }

        return $image;
    }

    public function getPriceFormattedAttribute()
    {
        return "Rp. " . number_format($this->attributes['price'], 2, ',', '.');
    }

    public function packetUsers()
    {
        return $this->hasMany(PacketUser::class, 'packet_id', 'id');
    }
}
