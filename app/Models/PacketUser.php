<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PacketUser extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = [
        'price_formatted',
        'status_formatted',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function packet()
    {
        return $this->hasOne(Packet::class, 'id', 'packet_id');
    }

    public function schedules()
    {
        return $this->hasMany(Schedule::class, 'packet_user_id', 'id');
    }

    public function getPriceFormattedAttribute()
    {
        return "Rp. " . number_format($this->attributes['price'], 2, ',', '.');
    }

    public function getStatusFormattedAttribute()
    {
        switch ($this->attributes['status']) {
            case 1:
                $status = 'ACTIVE';
                break;
            case 2:
                $status = 'REJECT';
                break;
            default:
                $status = 'INACTIVE';
                break;
        }

        return $status;
    }
}
