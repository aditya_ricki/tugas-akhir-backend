<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $guarded = [];
    protected $appends = [
        'photo_formatted',
        'id_card_formatted',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function packet_users()
    {
        return $this->hasMany(PacketUser::class, 'user_id', 'id');
    }

    public function getPhotoFormattedAttribute()
    {
        $photo = env('APP_URL') . 'noprofile.png';

        if ($this->attributes['photo']) {
            $photo = env('APP_URL') . $this->attributes['photo'];
        }

        return $photo;
    }

    public function getIdCardFormattedAttribute()
    {
        $idCard = '';

        if ($this->attributes['id_card']) {
            $idCard = env('APP_URL') . $this->attributes['id_card'];
        }

        return $idCard;
    }
}
