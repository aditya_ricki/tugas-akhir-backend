<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use App\Services\UserService;

class JwtMiddleware
{
    protected $userService;

    function __construct(UserService $userService)
    {
        $this->userService = $userService;
        $this->result = [
            'status' => 401,
            'message' => 'Unauthorized',
            'success' => false,
        ];
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $role = 'ALL')
    {
        try {
            if (!$request->hasHeader('Authorization')) {
                throw new InvalidArgumentException('No token provided');
            }

            $decodeToken = decodeTokenJwt($request->header('Authorization'));

            if ($role != 'ALL') {
                if (!in_array($decodeToken->user->role, explode('|', $role))) {
                    throw new InvalidArgumentException('Unauthorized');
                }
            }

            // if ($this->userService->readOne($decodeToken->user->id)->random_key != $decodeToken->user->random_key) {
            //     throw new InvalidArgumentException('Expired key');
            // }

            return $next($request);
        } catch (\Throwable $th) {
            Log::error($th->getMessage());
            return response()->json($this->result, $this->result['status']);
        }
    }
}
