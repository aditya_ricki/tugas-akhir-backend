<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\ProfileService;
use Throwable;

class ProfileController extends Controller
{
    protected $request, $service, $result;

    function __construct(Request $request, ProfileService $service)
    {
        $this->request = $request;
        $this->service = $service;
        $this->result = [
            'status' => 200,
            'message' => 'Success',
            'success' => true,
        ];
    }

    public function getProfile()
    {
        try {
            $this->result['data'] = $this->service->getProfile($this->request);
        } catch (Throwable $th) {
            $this->result = [
                'status' => 500,
                'message' => $th->getMessage(),
                'success' => false,
            ];
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function updateProfile()
    {
        try {
            $this->result['data'] = $this->service->updateProfile($this->request);
        } catch (Throwable $th) {
            $this->result = [
                'status' => 500,
                'message' => $th->getMessage(),
                'success' => false,
            ];
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function updatePassword()
    {
        try {
            $this->result['data'] = $this->service->updatePassword($this->request);
        } catch (Throwable $th) {
            $this->result = [
                'status' => 500,
                'message' => $th->getMessage(),
                'success' => false,
            ];
        }

        return response()->json($this->result, $this->result['status']);
    }
}
