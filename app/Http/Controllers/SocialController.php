<?php

namespace App\Http\Controllers;

use App\Services\SocialService;
use Throwable;
use Illuminate\Http\Request;
use App\Services\UserService;

class SocialController extends Controller
{
    protected $request, $service, $userService, $result;

    function __construct(Request $request, SocialService $service, UserService $userService)
    {
        $this->request = $request;
        $this->service = $service;
        $this->userService = $userService;
        $this->result = [
            'status' => 200,
            'message' => 'Success',
            'success' => true,
        ];
    }

    public function redirect($provider)
    {
        try {
            $this->result['data'] = $this->service->getTargetUrl($provider);
        } catch (Throwable $th) {
            $this->result = [
                'status' => 500,
                'message' => $th->getMessage(),
                'success' => false,
            ];
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function callback($provider)
    {
        try {
            $this->result['data'] = $this->service->checkUser($provider);
        } catch (Throwable $th) {
            $this->result = [
                'status' => 500,
                'message' => $th->getMessage(),
                'success' => false,
            ];
        }

        return response()->json($this->result, $this->result['status']);
    }
}
