<?php

namespace App\Http\Controllers\Customer;

use Throwable;
use Illuminate\Http\Request;
use App\Services\PacketUserService;
use App\Http\Controllers\Controller;
use App\Services\BillService;
use App\Services\ScheduleService;

class MyPacketController extends Controller
{
    protected $request, $service, $result, $scheduleService, $billService;

    function __construct(Request $request, PacketUserService $service, ScheduleService $scheduleService, BillService $billService)
    {
        $this->request = $request;
        $this->service = $service;
        $this->scheduleService = $scheduleService;
        $this->billService = $billService;
        $this->result = [
            'status' => 200,
            'message' => 'Success',
            'success' => true,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function myPacket()
    {
        try {
            $this->result['data'] = $this->service->readAllMyPacketPaginated($this->request);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function show($id)
    {
        try {
            $this->result['data'] = $this->service->readOne($id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function mySchedule($id)
    {
        try {
            $this->result['data'] = $this->scheduleService->readAllPaginatedByPacketUser($this->request, $id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function myBill($id)
    {
        try {
            $this->result['data'] = $this->billService->readAllPaginatedByPacketUser($this->request, $id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function store()
    {
        try {
            $this->result['data'] = $this->service->create($this->request);
        } catch (Throwable $th) {
            $this->result = [
                'status' => 500,
                'message' => $th->getMessage(),
                'success' => false,
            ];
        }

        return response()->json($this->result, $this->result['status']);
    }
}
