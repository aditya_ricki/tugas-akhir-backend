<?php

namespace App\Http\Controllers\Customer;

use Throwable;
use Illuminate\Http\Request;
use App\Services\BillService;
use App\Http\Controllers\Controller;

class BillController extends Controller
{
    protected $request, $service, $result;

    function __construct(Request $request, BillService $service)
    {
        $this->request = $request;
        $this->service = $service;
        $this->result = [
            'status' => 200,
            'message' => 'Success',
            'success' => true,
        ];
    }

    public function payment($billId)
    {
        try {
            $this->result['data'] = $this->service->payment($this->request, $billId);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function show($billId)
    {
        try {
            $this->result['data'] = $this->service->getStatusPayment($billId);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }
}
