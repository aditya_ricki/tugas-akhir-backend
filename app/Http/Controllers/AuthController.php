<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use Throwable;

class AuthController extends Controller
{
    protected $request, $service, $result;

    function __construct(Request $request, UserService $service)
    {
        $this->request = $request;
        $this->service = $service;
        $this->result = [
            'status' => 200,
            'message' => 'Success',
            'success' => true,
        ];
    }

    public function register()
    {
        try {
            $this->request->role = 'C';
            $this->result['data'] = $this->service->create($this->request);
        } catch (Throwable $th) {
            $this->result = [
                'status' => 500,
                'message' => $th->getMessage(),
                'success' => false,
            ];
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function checkOtp()
    {
        try {
            $this->result['data'] = $this->service->checkOtp($this->request);
        } catch (Throwable $th) {
            $this->result = [
                'status' => 500,
                'message' => $th->getMessage(),
                'success' => false,
            ];
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function resendOtp()
    {
        try {
            $this->result['data'] = $this->service->resendOtp($this->request);
        } catch (Throwable $th) {
            $this->result = [
                'status' => 500,
                'message' => $th->getMessage(),
                'success' => false,
            ];
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function login()
    {
        try {
            $this->result['data'] = $this->service->login($this->request);
        } catch (Throwable $th) {
            $this->result = [
                'status' => 500,
                'message' => $th->getMessage(),
                'success' => false,
            ];
        }

        return response()->json($this->result, $this->result['status']);
    }
}
