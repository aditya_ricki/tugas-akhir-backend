<?php

namespace App\Http\Controllers;

use Throwable;
use Illuminate\Http\Request;
use App\Services\PacketService;

class PacketController extends Controller
{
    protected $request, $service, $result;

    function __construct(Request $request, PacketService $service)
    {
        $this->request = $request;
        $this->service = $service;
        $this->result = [
            'status' => 200,
            'message' => 'Success',
            'success' => true,
        ];
    }

    public function index()
    {
        try {
            $this->result['data'] = $this->service->readAll($this->request);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }
}
