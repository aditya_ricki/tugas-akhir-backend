<?php

namespace App\Http\Controllers\Marketing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\UserService;
use Throwable;

class UserController extends Controller
{
    protected $request, $service, $result;

    function __construct(Request $request, UserService $service)
    {
        $this->request = $request;
        $this->service = $service;
        $this->result = [
            'status' => 200,
            'message' => 'Success',
            'success' => true,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $this->request->role = 'C';
            $this->result['data'] = $this->service->readAllPaginated($this->request);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        try {
            $this->request->role = 'C';
            $this->request->email_verified_at = now();
            $this->result['data'] = $this->service->create($this->request);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $this->result['data'] = $this->service->readOne($id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        try {
            $this->result['data'] = $this->service->update($this->request, $id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function updatePassword($id)
    {
        try {
            $this->result['data'] = $this->service->updatePassword($this->request, $id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->result['data'] = $this->service->delete($id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }
}
