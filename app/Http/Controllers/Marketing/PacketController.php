<?php

namespace App\Http\Controllers\Marketing;

use Throwable;
use Illuminate\Http\Request;
use App\Services\PacketService;
use App\Http\Controllers\Controller;

class PacketController extends Controller
{
    protected $request, $service, $result;

    function __construct(Request $request, PacketService $service)
    {
        $this->request = $request;
        $this->service = $service;
        $this->result = [
            'status' => 200,
            'message' => 'Success',
            'success' => true,
        ];
    }

    public function getAllDoesntHaveUser($userId)
    {
        try {
            // $this->result['data'] = $this->service->readAllDoesntHaveUser($this->request, $userId);
            $this->result['data'] = $this->service->readAll($this->request);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }
}
