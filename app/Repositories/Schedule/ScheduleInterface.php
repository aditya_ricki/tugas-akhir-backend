<?php

namespace App\Repositories\Schedule;

use App\Models\Bill;
use App\Models\Schedule;
use App\Models\PacketUser;

interface ScheduleInterface
{
    function create($request, Bill $bill);
    function readOne($id);
    function check(PacketUser $packetUser);
    function readAllByPacketUser($request, PacketUser $packetUser);
    function readAllPaginatedByPacketUser($request, PacketUser $packetUser);
    function readAllPaginated($request);
    function update($request, Schedule $model);
    function updateStatus($status, Schedule $model);
    function delete(Schedule $model);
}
