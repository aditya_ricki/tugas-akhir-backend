<?php

namespace App\Repositories\Bill;

use App\Models\Bill;
use App\Models\User;
use App\Models\PacketUser;
use App\Repositories\Bill\BillInterface;

class EloquentRepository implements BillInterface
{
    protected $model;

    public function __construct(Bill $model)
    {
        $this->model = $model;
    }

    public function create($request, PacketUser $packetUser)
    {
        return $this->model
            ->create([
                "description" => $request->description,
                "transaction_code" => "TRX-" . date("YmdHis") . substr(str_shuffle("QWERTYUIOPASDFGHJKLZXCVBNM"), 0, 2),
                "price" => $request->price,
                "status" => 0,
                "packet_user_id" => $packetUser->id,
                "type" => $request->type,
            ]);
    }

    public function readOne($id)
    {
        return $this->model
            ->with('packetUser.user')
            ->findOrFail($id);
    }

    public function readAllByUser($request, User $user)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->whereDate($request->search);
            })
            ->whereHas('packetUser', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })
            ->with('packetUser')
            ->orderBy('status')
            ->latest()
            ->get();
    }

    public function readAllByYear()
    {
        return $this->model
            ->where('paid_at', 'like', date('Y') . '%')
            ->whereNotIn('status', [0, 1, 3, 4, 5, 6])
            ->get();
    }

    public function readAllPaginatedByPacketUser($request, PacketUser $packetUser)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->whereDate($request->search);
            })
            ->where('packet_user_id', $packetUser->id)
            ->with('packetUser')
            ->orderBy('status')
            ->latest()
            ->paginate($request->per_page ?? 25);
    }

    public function readAll($request)
    {
        return $this->model
            ->when($request->from && $request->to, function ($query) use ($request) {
                $query->where('created_at', '>', $request->from)
                    ->where('created_at', '<', $request->to);
            })
            ->when($request->type, function ($query) use ($request) {
                $query->where('type', $request->type);
            })
            ->when($request->is_paid == 'paid', function ($query) use ($request) {
                $query->where('status', 2);
            })
            ->when($request->is_paid == 'unpaid', function ($query) use ($request) {
                $query->where('status', '!=', 2);
            })
            ->whereHas('packetUser', function ($query) use ($request) {
                $query->whereHas('user', function ($query) use ($request) {
                    $query->when($request->name, function ($query) use ($request) {
                        $query->where('name', 'like', "%$request->name%");
                    });
                });
            })
            ->with('packetUser.user')
            ->orderBy('status')
            ->latest()
            ->get();
    }

    public function readAllPaginated($request)
    {
        return $this->model
            ->when($request->from && $request->to, function ($query) use ($request) {
                $query->where('created_at', '>', $request->from)
                    ->where('created_at', '<', $request->to);
            })
            ->when($request->type, function ($query) use ($request) {
                $query->where('type', $request->type);
            })
            ->when($request->is_paid == 'paid', function ($query) use ($request) {
                $query->where('status', 2);
            })
            ->when($request->is_paid == 'unpaid', function ($query) use ($request) {
                $query->where('status', '!=', 2);
            })
            ->whereHas('packetUser', function ($query) use ($request) {
                $query->whereHas('user', function ($query) use ($request) {
                    $query->when($request->name, function ($query) use ($request) {
                        $query->where('name', 'like', "%$request->name%");
                    });
                });
            })
            ->with('packetUser.user')
            ->orderBy('status')
            ->latest()
            ->paginate($request->per_page ?? 25);
    }

    public function update($request, Bill $model)
    {
        return $model->update([
            "description" => $request->description ?? $model->description,
            "price" => $request->price ?? $model->price,
            "status" => $request->status ?? $model->status,
        ]);
    }

    public function updateStatus($status, Bill $model)
    {
        return $model->update([
            "status" => $status,
        ]);
    }

    public function delete(Bill $model)
    {
        return $model->delete();
    }

    public function payment($paymentUrl, Bill $model)
    {
        return $model->update([
            'payment_url' => $paymentUrl,
            'status' => 1,
        ]);
    }

    public function manualPayment(Bill $model)
    {
        return $model->update([
            'status' => 2,
            'method' => 'CASH',
            'paid_at' => now(),
        ]);
    }

    public function getMonthlyBill($month)
    {
        return $this->model
            ->whereType('MONTHLY')
            ->where('created_at', 'like', "$month%")
            ->get();
    }
}
