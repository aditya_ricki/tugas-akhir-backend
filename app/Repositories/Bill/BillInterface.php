<?php

namespace App\Repositories\Bill;

use App\Models\Bill;
use App\Models\PacketUser;

interface BillInterface
{
    function create($request, PacketUser $packetUser);
    function readOne($id);
    function readAllPaginatedByPacketUser($request, PacketUser $packetUser);
    function readAll($request);
    function readAllPaginated($request);
    function update($request, Bill $model);
    function updateStatus($status, Bill $model);
    function delete(Bill $model);
    function payment($paymentUrl, Bill $model);
    function manualPayment(Bill $model);
    function getMonthlyBill($month);
}
