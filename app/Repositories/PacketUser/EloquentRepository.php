<?php

namespace App\Repositories\PacketUser;

use App\Models\Packet;
use App\Models\PacketUser;
use App\Models\User;
use App\Repositories\PacketUser\PacketUserInterface;

class EloquentRepository implements PacketUserInterface
{
    protected $model;

    public function __construct(PacketUser $model)
    {
        $this->model = $model;
    }

    public function create($request, User $user, Packet $packet)
    {
        return $this->model
            ->create([
                "user_id" => $user->id,
                "packet_id" => $packet->id,
                "longitude" => substr($request->longitude, 0, 12),
                "latitude" => substr($request->latitude, 0, 12),
                "price" => $packet->price,
                "status" => 0
            ]);
    }

    public function readOne($id)
    {
        return $this->model
            ->with('packet', 'user')
            ->whereId($id)
            ->first();
    }

    public function readOneByUserPacket($userId, $packetId)
    {
        return $this->model
            ->with('packet', 'user')
            ->whereUserId($userId)
            ->wherePacketId($packetId)
            ->first();
    }

    public function readAllByUser($request, User $user)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->wherehas('packet', function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->search . '%');
                });
            })
            ->with('packet', 'user')
            ->where('user_id', $user->id)
            ->latest()
            ->get();
    }

    public function readAllPaginatedByUser($request, User $user)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->whereHas('packet', function ($query) use ($request) {
                    $query->where('name', 'like', "%$request->search%");
                });
            })
            ->with('packet', 'user')
            ->where('user_id', $user->id)
            ->latest()
            ->paginate($request->per_page ?? 25);
    }

    public function readAll($request)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->wherehas('packet', function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->search . '%');
                });
            })
            ->where('status', 1)
            ->with('packet', 'user')
            ->latest()
            ->get();
    }

    public function readAllPaginated($request)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->wherehas('packet', function ($query) use ($request) {
                    $query->where('name', 'like', '%' . $request->search . '%');
                })->orWhereHas('user', function ($query) use ($request) {
                    $query->where('name', 'like', "%$request->search%");
                });
            })
            ->with('packet', 'user')
            ->latest()
            ->paginate($request->per_page ?? 25);
    }

    public function update($request, PacketUser $model)
    {
        return $model->update([
            "longitude" => substr($request->longitude, 0, 12),
            "latitude" => substr($request->latitude, 0, 12),
            "price" => $request->price,
        ]);
    }

    public function updateStatus($request, PacketUser $model)
    {
        return $model->update([
            "status" => $request->status,
            "notes" => $request->notes,
        ]);
    }

    public function delete(PacketUser $model)
    {
        return $model->delete();
    }
}
