<?php

namespace App\Repositories\Packet;

use App\Models\Packet;

interface PacketInterface
{
    function create($request);
    function readOne($id);
    function readAll($request);
    function readAllPaginated($request);
    function update($request, Packet $model);
    function delete(Packet $model);
}
