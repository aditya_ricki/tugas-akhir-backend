<?php

namespace App\Repositories\Packet;

use App\Models\User;
use App\Models\Packet;
use App\Repositories\Packet\PacketInterface;

class EloquentRepository implements PacketInterface
{
    protected $model;

    public function __construct(Packet $model)
    {
        $this->model = $model;
    }

    public function create($request)
    {
        return $this->model
            ->create([
                "name" => $request->name,
                "description" => $request->description,
                "price" => $request->price,
                "image" => $request->path_image,
            ]);
    }

    public function readOne($id)
    {
        return $this->model
            ->findOrFail($id);
    }

    public function readAll($request)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->where("name", "like", "%$request->search%");
            })
            ->latest()
            ->get();
    }

    public function readAllDoesntHaveUser($request, User $user)
    {
        return $this->model
            ->whereDoesntHave('packetUsers', function ($query) use ($user) {
                $query->where('user_id', $user->id);
            })
            ->when($request->search, function ($query) use ($request) {
                $query->where("name", "like", "%$request->search%");
            })
            ->latest()
            ->get();
    }

    public function readAllPaginated($request)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->where("name", "like", "%$request->search%");
            })
            ->latest()
            ->paginate($request->per_page ?? 25);
    }

    public function update($request, Packet $model)
    {
        return $model->update([
            "name" => $request->name,
            "description" => $request->description,
            "price" => $request->price,
            "image" => $request->path_image ?? $model->image,
        ]);
    }

    public function delete(Packet $model)
    {
        return $model->delete();
    }
}
