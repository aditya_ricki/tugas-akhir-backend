<?php

namespace App\Repositories\User;

use App\Models\User;

interface UserInterface
{
    function create($request);
    function readOne($id);
    function readAll($request);
    function readAllPaginated($request);
    function update($request, User $model);
    function updatePassword($request, User $model);
    function delete(User $model);
}
