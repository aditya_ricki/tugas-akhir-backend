<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\MidtransController;
use App\Http\Controllers\SocialController;
use App\Http\Controllers\DashboardController;

use App\Http\Controllers\Customer\MyPacketController;
use App\Http\Controllers\Customer\BillController as CustomerBillController;

use App\Http\Controllers\Tech\PacketController as TechPacketController;
use App\Http\Controllers\Tech\ScheduleController as TechScheduleController;
use App\Http\Controllers\Tech\PacketUserController as TechPacketUserController;

use App\Http\Controllers\Finance\ScheduleController as FinanceScheduleController;
use App\Http\Controllers\Finance\BillController as FinanceBillController;
use App\Http\Controllers\Finance\PacketUserController as FinancePacketUserController;

use App\Http\Controllers\Marketing\UserController as MarketingUserController;
use App\Http\Controllers\Marketing\PacketController as MarketingPacketController;
use App\Http\Controllers\Marketing\PacketUserController as MarketingPacketUserController;
use App\Http\Controllers\PacketController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::prefix('auth')->group(function () {
        Route::post('login', [AuthController::class, 'login']);
        Route::post('register', [AuthController::class, 'register']);
        Route::post('check-otp', [AuthController::class, 'checkOtp']);
        Route::post('resend-otp', [AuthController::class, 'resendOtp']);

        Route::prefix('social')->group(function () {
            Route::get('/{provider}/redirect', [SocialController::class, 'redirect']);
            Route::post('/{provider}/callback', [SocialController::class, 'callback']);
        });
    });

    /**
     * T = teknisi
     * C = customer
     * M = marketing
     * F = finance
     */
    Route::get('packet', [PacketController::class, 'index']);

    Route::middleware(['jwt:C|T|M|F'])->group(function () {
        Route::group(['prefix' => 'profile'], function () {
            Route::get('/', [ProfileController::class, 'getProfile']);
            Route::put('/', [ProfileController::class, 'updateProfile']);
            Route::put('/password', [ProfileController::class, 'updatePassword']);
        });
    });

    Route::prefix('marketing')->middleware(['jwt:M'])->group(function () {
        Route::get('dashboard', [DashboardController::class, 'marketing']);
        Route::resource('user', MarketingUserController::class)->except(['create', 'edit']);
        Route::put('user/{user}/password', [MarketingUserController::class, 'updatePassword']);
        Route::get('packet/{userId}', [MarketingPacketController::class, 'getAllDoesntHaveUser']);
        Route::get('packet-user/{userId}/by-user', [MarketingPacketUserController::class, 'getByUser']);
        Route::resource('packet-user', MarketingPacketUserController::class)->except(['index', 'create', 'edit']);
    });

    Route::prefix('tech')->middleware(['jwt:T'])->group(function () {
        Route::get('dashboard', [DashboardController::class, 'tech']);
        Route::resource('packet', TechPacketController::class)->except(['create', 'edit']);
        Route::put('packet-user/{id}/update-status', [TechPacketUserController::class, 'updateStatus']);
        Route::resource('packet-user', TechPacketUserController::class)->only(['index', 'show']);
        Route::prefix('schedule')->group(function () {
            Route::get('/{packetUserId}/by-packet-user', [TechScheduleController::class, 'getByPacketUser']);
            Route::post('/', [TechScheduleController::class, 'store']);
            Route::get('/{id}', [TechScheduleController::class, 'show']);
            Route::put('/{id}', [TechScheduleController::class, 'update']);
            Route::put('/{id}/update-status', [TechScheduleController::class, 'updateStatus']);
            Route::delete('/{id}', [TechScheduleController::class, 'destroy']);
        });
    });

    Route::prefix('finance')->middleware(['jwt:F'])->group(function () {
        Route::get('dashboard', [DashboardController::class, 'finance']);
        Route::resource('packet-user', FinancePacketUserController::class)->only(['index', 'show']);

        Route::prefix('schedule')->group(function () {
            Route::get('/', [FinanceScheduleController::class, 'index']);
            Route::put('/{id}/approve', [FinanceScheduleController::class, 'approve']);
            Route::get('/{packetUserId}/by-packet-user', [FinanceScheduleController::class, 'getByPacketUser']);
        });

        Route::prefix('bill')->group(function () {
            Route::get('/export', [FinanceBillController::class, 'export']);
            Route::get('/', [FinanceBillController::class, 'index']);
            Route::get('/{packetUserId}/by-packet-user', [FinanceBillController::class, 'getByPacketUser']);
            Route::get('/{id}', [FinanceBillController::class, 'show']);
            Route::put('/{id}/update-status', [FinanceBillController::class, 'update']);
            Route::post('/{id}/manual-payment', [FinanceBillController::class, 'manualPayment']);
            Route::post('/', [FinanceBillController::class, 'manualStore']);
        });
    });

    Route::prefix('customer')->middleware(['jwt:C'])->group(function () {
        Route::get('dashboard', [DashboardController::class, 'customer']);
        Route::prefix('my-packet')->group(function () {
            Route::get('/', [MyPacketController::class, 'myPacket']);
            Route::post('/', [MyPacketController::class, 'store']);
            Route::get('/{id}', [MyPacketController::class, 'show']);
            Route::get('/{id}/schedule', [MyPacketController::class, 'mySchedule']);
            Route::get('/{id}/bill', [MyPacketController::class, 'myBill']);
        });

        Route::prefix('payment')->group(function () {
            Route::get('/{billId}', [CustomerBillController::class, 'show']);
            Route::post('/{billId}', [CustomerBillController::class, 'payment']);
        });
    });

    Route::prefix('callback')->group(function () {
        Route::post('midtrans-notification', [MidtransController::class, 'notificationHandler']);
    });
});
