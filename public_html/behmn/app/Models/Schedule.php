<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = [
        'date_formatted',
        'status_formatted',
        'description_formatted',
    ];

    public function bill()
    {
        return $this->hasOne(Bill::class, 'id', 'bill_id');
    }

    public function getDateFormattedAttribute()
    {
        return date('D, d M Y', strtotime($this->attributes['date']));
    }

    public function getStatusFormattedAttribute()
    {
        switch ($this->attributes['status']) {
            case 1:
                $status = 'APPROVED';
                break;
            case 2:
                $status = 'DONE';
                break;
            default:
                $status = 'PENDING';
                break;
        }

        return $status;
    }

    public function getDescriptionFormattedAttribute()
    {
        if (strlen($this->attributes['description']) > 21) {
            return substr($this->attributes['description'], 0, 21) . "...";
        }

        return $this->attributes['description'];
    }
}
