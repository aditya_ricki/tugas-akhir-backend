<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    use HasFactory;

    protected $guarded = [];
    protected $appends = [
        'price_formatted',
        'created_at_formatted',
        'status_formatted',
        'description_formatted',
    ];

    public function getPriceFormattedAttribute()
    {
        return "Rp. " . number_format($this->attributes['price'], 2, ',', '.');
    }

    public function getStatusFormattedAttribute()
    {
        switch ($this->attributes['status']) {
            case 0:
                $status = "NO PAYMENT SELECTED";
                break;
            case 1:
                $status = "WAITING PAYMENT";
                break;
            case 3:
                $status = "DENIED";
                break;
            case 4:
                $status = "EXPIRED";
                break;
            case 5:
                $status = "CANCELED";
                break;
            case 6:
                $status = "CHALLENGED";
                break;

            default:
                $status = "PAYMENT SUCCESS";
                break;
        }

        return $status;
    }

    public function packetUser()
    {
        return $this->hasOne(PacketUser::class, 'id', 'packet_user_id');
    }

    public function getDescriptionFormattedAttribute()
    {
        if (strlen($this->attributes['description']) > 21) {
            return substr($this->attributes['description'], 0, 21) . "...";
        }

        return $this->attributes['description'];
    }

    public function getCreatedAtFormattedAttribute()
    {
        return date("Y-m-d H:i:s", strtotime($this->attributes['created_at']));
    }
}
