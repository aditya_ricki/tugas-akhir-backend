<?php

namespace App\Repositories\Schedule;

use App\Models\Bill;
use App\Models\Schedule;
use App\Models\PacketUser;
use App\Repositories\Schedule\ScheduleInterface;
use Illuminate\Support\Facades\Schema;

class EloquentRepository implements ScheduleInterface
{
    protected $model;

    public function __construct(Schedule $model)
    {
        $this->model = $model;
    }

    public function create($request, Bill $bill)
    {
        return $this->model
            ->create([
                "date" => $request->date,
                "bill_id" => $bill->id,
                "description" => $request->description,
                "status" => 0,
            ]);
    }

    public function readOne($id)
    {
        return $this->model
            ->findOrFail($id);
    }

    public function check(PacketUser $packetUser)
    {
        return $this->model
            ->whereHas('bill', function ($query) use ($packetUser) {
                return $query->where('packet_user_id', $packetUser->id);
            })
            ->whereIn('status', [0, 1])
            ->first();
    }

    public function readAllByPacketUser($request, PacketUser $packetUser)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->where('description', 'like', "%$request->search%");
            })
            ->whereHas('bill', function ($query) use ($packetUser) {
                return $query->where('packet_user_id', $packetUser->id);
            })
            ->with(['bill.packetUser' => function ($query) {
                $query->with('user', 'packet');
            }])
            ->orderBy('status')
            ->latest()
            ->get();
    }

    public function readAllPaginatedByPacketUser($request, PacketUser $packetUser)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->where('description', 'like', "%$request->search%");
            })
            ->whereHas('bill', function ($query) use ($packetUser) {
                return $query->where('packet_user_id', $packetUser->id);
            })
            ->with(['bill.packetUser' => function ($query) {
                $query->with('user', 'packet');
            }])
            ->orderBy('status')
            ->latest()
            ->paginate($request->per_page ?? 25);
    }

    public function readAllPaginated($request)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->where('description', 'like', "%$request->search%")
                    ->orWhereHas('bill.packetUser.user', function ($query) use ($request) {
                        return $query->where('name', 'like', "%$request->search%");
                    });
            })
            ->with(['bill.packetUser' => function ($query) {
                $query->with('user', 'packet');
            }])
            ->orderBy('status')
            ->latest()
            ->paginate($request->per_page ?? 25);
    }

    public function readAll($request)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->where('description', 'like', "%$request->search%")
                    ->orWhereHas('bill.packetUser.user', function ($query) use ($request) {
                        return $query->where('name', 'like', "%$request->search%");
                    });
            })
            ->with(['bill.packetUser' => function ($query) {
                $query->with('user', 'packet');
            }])
            ->orderBy('status')
            ->latest()
            ->get();
    }

    public function update($request, Schedule $model)
    {
        return $model->update([
            "description" => $request->description,
            "date" => $request->date,
        ]);
    }

    public function updateStatus($status, Schedule $model)
    {
        return $model->update([
            "status" => $status,
        ]);
    }

    public function delete(Schedule $model)
    {
        return $model->delete();
    }
}
