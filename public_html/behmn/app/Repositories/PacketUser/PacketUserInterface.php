<?php

namespace App\Repositories\PacketUser;

use App\Models\User;
use App\Models\Packet;
use App\Models\PacketUser;

interface PacketUserInterface
{
    function create($request, User $user, Packet $packet);
    function readOne($id);
    function readAll($request);
    function readAllPaginated($request);
    function update($request, PacketUser $model);
    function delete(PacketUser $model);
}
