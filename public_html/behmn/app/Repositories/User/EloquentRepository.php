<?php

namespace App\Repositories\User;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Repositories\User\UserInterface;

class EloquentRepository implements UserInterface
{
    protected $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function create($request)
    {
        return $this->model
            ->create([
                "name" => $request->name,
                "email" => $request->email,
                "password" => Hash::make($request->password),
                "role" => $request->role,
                "phone" => $request->phone,
                "whatsapp" => $request->whatsapp,
                "address" => $request->address,
                "photo" => $request->path_photo,
                "id_card" => $request->path_id_card,
                "otp" => substr(str_shuffle("1234567890"), 0, 6),
                "email_verified_at" => $request->email_verified_at,
            ]);
    }

    public function readOne($id)
    {
        return $this->model
            ->findOrFail($id);
    }

    public function readOneByEmail($email)
    {
        return $this->model
            ->where('email', $email)
            ->first();
    }

    public function readAll($request)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->where("name", "like", "%$request->search%")
                    ->orWhere("email", "like", "%$request->search%")
                    ->orWhere("phone", "like", "%$request->search%")
                    ->orWhere("whatsapp", "like", "%$request->search%");
            })
            ->when($request->role, function ($query) use ($request) {
                $query->where("role", $request->role);
            })
            ->withCount('packet_users')
            ->latest()
            ->get();
    }

    public function readAllByRole(array $role)
    {
        return $this->model
            ->whereIn('role', $role)
            ->get();
    }

    public function readAllPaginated($request)
    {
        return $this->model
            ->when($request->search, function ($query) use ($request) {
                $query->where("name", "like", "%$request->search%")
                    ->orWhere("email", "like", "%$request->search%")
                    ->orWhere("phone", "like", "%$request->search%")
                    ->orWhere("whatsapp", "like", "%$request->search%");
            })
            ->when($request->role, function ($query) use ($request) {
                $query->where("role", $request->role);
            })
            ->latest()
            ->paginate($request->per_page ?? 25);
    }

    public function update($request, User $model)
    {
        return $model->update([
            "name" => $request->name,
            "phone" => $request->phone,
            "whatsapp" => $request->whatsapp,
            "address" => $request->address,
            "photo" => $request->path_photo ?? $model->photo,
            "id_card" => $request->path_id_card ?? $model->id_card,
            "random_key" => substr(str_shuffle("QWERTYUIOPASDFGHJKLZXCVBNM"), 0, 20),
        ]);
    }

    public function updatePassword($request, User $model)
    {
        return $model->update([
            "password" => Hash::make($request->password),
            "random_key" => substr(str_shuffle("QWERTYUIOPASDFGHJKLZXCVBNM"), 0, 20),
        ]);
    }

    public function delete(User $model)
    {
        return $model->delete();
    }

    public function checkOtp($request, User $model)
    {
        return $model->otp == $request->otp;
    }

    public function updateOtp(User $model)
    {
        return $model->update([
            "otp" => substr(str_shuffle("1234567890"), 0, 6),
        ]);
    }

    public function updateRandomKey(User $model)
    {
        return $model->update([
            "random_key" => substr(str_shuffle("QWERTYUIOPASDFGHJKLZXCVBNM"), 0, 20),
        ]);
    }
}
