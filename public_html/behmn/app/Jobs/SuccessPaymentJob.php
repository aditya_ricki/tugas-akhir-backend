<?php

namespace App\Jobs;

use App\Models\Bill;
use Illuminate\Bus\Queueable;
use App\Mail\SuccessPaymentMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class SuccessPaymentJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $bill, $finances;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Bill $bill, Collection $finances)
    {
        $this->bill = $bill;
        $this->finances = $finances;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->finances as $key => $finance) {
            Mail::to($finance->email)->send(new SuccessPaymentMail($this->bill));
        }

        Mail::to($this->bill->packetUser->user->email)->send(new SuccessPaymentMail($this->bill));
    }
}
