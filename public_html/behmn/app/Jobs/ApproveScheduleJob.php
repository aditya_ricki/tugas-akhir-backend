<?php

namespace App\Jobs;

use App\Models\Schedule;
use Illuminate\Bus\Queueable;
use App\Mail\ApproveScheduleMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Queue\ShouldBeUnique;

class ApproveScheduleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $schedule, $techs;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Schedule $schedule, Collection $techs)
    {
        $this->schedule = $schedule;
        $this->techs = $techs;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->techs as $tech) {
            Mail::to($tech)->send(new ApproveScheduleMail($this->schedule));
        }

        Mail::to($this->schedule->bill->packetUser->user->email)->send(new ApproveScheduleMail($this->schedule));
    }
}
