<?php

namespace App\Console\Commands;

use Throwable;
use Illuminate\Http\Request;
use App\Services\BillService;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class BillCron extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bill:cron';
    protected $request, $service;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command for generate bill for each month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Request $request, BillService $service)
    {
        $this->request = $request;
        $this->service = $service;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            Log::info("================== BILL CRON IS RUN ==================");
            // $this->service->generateMonthlyBill($this->request);
        } catch (Throwable $th) {
            Log::info("================== BILL CRON ==================");
            Log::info($th->getMessage());
            Log::info("================== BILL CRON ==================");
        }
    }
}
