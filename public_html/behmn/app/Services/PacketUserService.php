<?php

namespace App\Services;

use App\Jobs\NewCustomerJob;
use App\Repositories\Packet\EloquentRepository as PacketEloquentRepository;
use Throwable;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Repositories\PacketUser\EloquentRepository;
use App\Repositories\User\EloquentRepository as UserEloquentRepository;

class PacketUserService
{
    protected $repository, $userRepository, $packetRepository, $job;

    public function __construct(EloquentRepository $repository, UserEloquentRepository $userRepository, PacketEloquentRepository $packetRepository, NewCustomerJob $job)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->packetRepository = $packetRepository;
        $this->job = $job;
    }

    public function create($request)
    {
        $validator = Validator::make($request->all(), [
            "user_id" => "required",
            "packet_id" => "required",
            "longitude" => "required",
            "latitude" => "required",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $user = $this->userRepository->readOne($request->user_id);

            if (!$user || $user->role != "C") {
                throw new InvalidArgumentException("User not found");
            }

            $packet = $this->packetRepository->readOne($request->packet_id);

            if (!$packet) {
                throw new InvalidArgumentException("Packet not found");
            }

            $create = $this->repository->create($request, $user, $packet);

            // notif email ke tech
            dispatch(new NewCustomerJob($create, $this->userRepository->readAllByRole(["T"])));
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }

        DB::commit();
        return $create;
    }

    public function readOne($id)
    {
        try {
            return $this->repository->readOne($id);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to read data by id");
        }
    }

    public function readAllByUser($request, $userId)
    {
        try {
            $user = $this->userRepository->readOne($userId);

            if (!$user) {
                throw new InvalidArgumentException("User not found");
            }

            return $this->repository->readAllByUser($request, $user);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException('Unable to read all data');
        }
    }

    public function readAllPaginatedByUser($request, $userId)
    {
        try {
            $user = $this->userRepository->readOne($userId);

            if (!$user) {
                throw new InvalidArgumentException("User not found");
            }

            return $this->repository->readAllPaginatedByUser($request, $user);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to read all paginated data');
        }
    }

    public function readAllMyPacketPaginated($request)
    {
        try {
            $user = $this->userRepository->readOne(decodeTokenJwt($request->header('Authorization'))->user->id);

            if (!$user) {
                throw new InvalidArgumentException("User not found");
            }

            return $this->repository->readAllPaginatedByUser($request, $user);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to read all paginated data');
        }
    }

    public function readAll($request)
    {
        try {
            return $this->repository->readAll($request);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException('Unable to read all data');
        }
    }

    public function readAllPaginated($request)
    {
        try {
            return $this->repository->readAllPaginated($request);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to read all paginated data');
        }
    }

    public function update($request, $id)
    {
        $validator = Validator::make($request->all(), [
            "longitude" => "required",
            "latitude" => "required",
            "price" => "required",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $update = $this->repository->update($request, $data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to update data");
        }

        DB::commit();
        return $update;
    }

    public function updateStatus($request, $id)
    {
        $validator = Validator::make($request->all(), [
            "status" => "required|in:0,1,2",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $update = $this->repository->updateStatus($request, $data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to update data");
        }

        DB::commit();
        return $update;
    }

    public function delete($id)
    {
        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $delete = $this->repository->delete($data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to delete data");
        }

        DB::commit();
        return $delete;
    }
}
