<?php

namespace App\Services;

use Throwable;
use App\Jobs\SendOtpJob;
use App\Repositories\Bill\EloquentRepository as BillRepository;
use App\Repositories\Packet\EloquentRepository as PacketRepository;
use App\Repositories\PacketUser\EloquentRepository as PacketUserRepository;
use App\Repositories\Schedule\EloquentRepository as ScheduleRepository;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Repositories\User\EloquentRepository as UserRepository;
use Carbon\Carbon;

class DashboardService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository, PacketUserRepository $packetUserRepository, BillRepository $billRepository, ScheduleRepository $scheduleRepository, PacketRepository $packetRepository)
    {
        $this->userRepository = $userRepository;
        $this->packetUserRepository = $packetUserRepository;
        $this->billRepository = $billRepository;
        $this->scheduleRepository = $scheduleRepository;
        $this->packetRepository = $packetRepository;
    }

    public function getForMarketing($request)
    {
        try {
            $customers = $this->userRepository->readAll($request);
            $totalCustomer = count($customers->where('role', 'C'));
            $activeCustomer = count($customers->where('role', 'C')->where('packet_users_count', '!=', 0));
            $prospectiveCustomer = count($customers->where('role', 'C')->where('packet_users_count', 0));
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to get data dashboard");
        }

        return [
            "total_customer" => $totalCustomer,
            "active_customer" => $activeCustomer,
            "prospective_customer" => $prospectiveCustomer,
        ];
    }

    public function getForCustomer($request)
    {
        try {
            $user = $this->userRepository->readOne(decodeTokenJwt($request->header('Authorization'))->user->id);

            if (!$user) {
                throw new InvalidArgumentException("User not found");
            }

            $packetUsers = $this->packetUserRepository->readAllByUser($request, $user);
            $activePacket = count($packetUsers->where('status', 1));
            $nonactivePacket = count($packetUsers->where('status', '!=', 1));

            $bills = $this->billRepository->readAllByUser($request, $user);
            $paidOff = count($bills->where('paid_at', '!=', null));
            $nonPaidOff = count($bills->where('paid_at', null));
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to get data dashboard");
        }

        return [
            "active_packet" => $activePacket,
            "nonactive_packet" => $nonactivePacket,
            "paid_off" => $paidOff,
            "non_paid_off" => $nonPaidOff,
        ];
    }

    public function getForTech($request)
    {

        try {
            $schedules = $this->scheduleRepository->readAll($request);
            $date = Carbon::now()->format('Y-m-d');
            $today = count($schedules->filter(function ($schedule) use ($date) {
                return $schedule->date == $date;
            }));
            $unfinished = count($schedules->where('status', '!=', 2));
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to get data dashboard");
        }

        return [
            "today" => $today,
            "unfinished" => $unfinished,
        ];
    }

    public function getForFinance($request)
    {

        try {
            $packetUsers = $this->packetUserRepository->readAll($request);
            $activeCustomer = count($packetUsers->where('status', 1));
            $inactiveCustomer = count($packetUsers->whereNotIn('status', [1, 2]));
            $rejectedCustomer = count($packetUsers->where('status', 2));

            $xaxis = [];
            $series = [];
            $bills = $this->billRepository->readAllByYear()->groupBy(function ($item) {
                return $item->created_at->format('M');
            });

            foreach (["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"] as $month) {
                $income = 0;
                foreach ($bills as $key => $bill) {
                    if ($key == $month) {
                        foreach ($bill as $data) {
                            $income += $data->price;
                        }
                    }
                }

                array_push($series, $income);
                array_push($xaxis, $month);
            }

            $labelsPie = [];
            $seriesPie = [];

            foreach ($this->packetRepository->readAll($request) as $packet) {
                $totalUser = 0;
                foreach ($this->packetUserRepository->readAll($request)->groupBy('packet_id') as $key => $packetUser) {
                    if ($key == $packet->id) {
                        $totalUser += count($packetUser);
                    }
                }
                array_push($seriesPie, $totalUser);
                array_push($labelsPie, $packet->name);
            }
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to get data dashboard");
        }

        return [
            "active_customer" => $activeCustomer,
            "inactive_customer" => $inactiveCustomer,
            "rejected_customer" => $rejectedCustomer,
            "bills" => $bills,
            "line_chart" => [
                "xaxis" => $xaxis,
                "series" => $series,
            ],
            "pie_chart" => [
                "labels" => $labelsPie,
                "series" => $seriesPie,
            ],
        ];
    }
}
