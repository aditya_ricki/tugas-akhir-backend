<?php

namespace App\Services;

use Throwable;
use App\Jobs\SendOtpJob;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Repositories\User\EloquentRepository;

class UserService
{
    protected $repository;

    public function __construct(EloquentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function create($request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "email" => "required|email|unique:users",
            "password" => "required|string|min:8",
            "phone" => "required|string|unique:users",
            "whatsapp" => "required|string|unique:users",
            "address" => "required",
            "photo" => "required",
            "id_card" => "required",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $fileName = str_shuffle("QWERTYUIOPASDFGHJKLZXCVBNM") . ".png";
            $request->path_photo = uploadFileLocal("users/photo/$fileName", $request->photo);
            $request->path_id_card = uploadFileLocal("users/id_card/$fileName", $request->id_card);

            $create = $this->repository->create($request);

            dispatch(new SendOtpJob($create));
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to create data");
        }

        DB::commit();
        return $create;
    }

    public function readOne($id)
    {
        try {
            return $this->repository->readOne($id);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to read data by id");
        }
    }

    public function readAll($request)
    {
        try {
            return $this->repository->readAll($request);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException('Unable to read all data');
        }
    }

    public function readAllByRole($role)
    {
        try {
            return $this->repository->readAllByRole($role);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException('Unable to read all data');
        }
    }

    public function readAllPaginated($request)
    {
        try {
            return $this->repository->readAllPaginated($request);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to read all paginated data');
        }
    }

    public function update($request, $id)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "phone" => "required|unique:users,phone,$id",
            "whatsapp" => "required|unique:users,whatsapp,$id",
            "address" => "required",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $fileName = str_shuffle("QWERTYUIOPASDFGHJKLZXCVBNM") . ".png";

            if ($request->photo) {
                $request->path_photo = uploadFileLocal("users/photo/$fileName", $request->photo);
            }

            if ($request->id_card) {
                $request->path_id_card = uploadFileLocal("users/id_card/$fileName", $request->id_card);
            }

            $update = $this->repository->update($request, $data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to update data");
        }

        DB::commit();
        return $update;
    }

    public function updatePassword($request, $id)
    {
        $validator = Validator::make($request->all(), [
            "password" => "required|string|min:8",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $updatePassword = $this->repository->updatePassword($request, $data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to update password");
        }

        DB::commit();
        return $updatePassword;
    }

    public function delete($id)
    {
        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $delete = $this->repository->delete($data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to delete data");
        }

        DB::commit();
        return $delete;
    }

    public function checkOtp($request)
    {
        $validator = Validator::make($request->all(), [
            "otp" => "required",
            "email" => "required|email",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        try {
            $data = $this->repository->readOneByEmail($request->email);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $checkOtp = $this->repository->checkOtp($request, $data);

            if (!$checkOtp) {
                throw new InvalidArgumentException("OTP code invalid");
            }

            $data->markEmailAsVerified();
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }

        return $checkOtp;
    }

    public function resendOtp($request)
    {
        $validator = Validator::make($request->all(), [
            "email" => "required|email",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $data = $this->repository->readOneByEmail($request->email);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $resend = $this->repository->updateOtp($data);

            dispatch(new SendOtpJob($data));
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }

        DB::commit();
        return $resend;
    }

    public function login($request)
    {
        $validator = Validator::make($request->all(), [
            "email" => "required|email",
            "password" => "required|string|min:8",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        try {
            $data = $this->repository->readOneByEmail($request->email);

            if (!$data) {
                throw new InvalidArgumentException('User not registered');
            }

            if (!$data->hasVerifiedEmail()) {
                throw new InvalidArgumentException('Email not verified');
            }

            $check = Hash::check($request->password, $data->password);

            if (!$check) {
                throw new InvalidArgumentException('Password is wrong');
            }

            $this->repository->updateRandomKey($data);

            $token = generateTokenJwt($data);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }

        return $token;
    }
}
