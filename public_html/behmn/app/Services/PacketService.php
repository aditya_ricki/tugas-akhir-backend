<?php

namespace App\Services;

use Throwable;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Packet\EloquentRepository;
use App\Repositories\User\EloquentRepository as UserEloquentRepository;

class PacketService
{
    protected $repository, $userRepository;

    public function __construct(EloquentRepository $repository, UserEloquentRepository $userRepository)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
    }

    public function create($request)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required|unique:packets|string|max:255",
            "description" => "required",
            "price" => "required",
            "image" => "required",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $fileName = str_shuffle("QWERTYUIOPASDFGHJKLZXCVBNM") . ".png";
            $request->path_image = uploadFileLocal("packets/image/$fileName", $request->image);

            $create = $this->repository->create($request);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to create data");
        }

        DB::commit();
        return $create;
    }

    public function readOne($id)
    {
        try {
            return $this->repository->readOne($id);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to read data by id");
        }
    }

    public function readAll($request)
    {
        try {
            return $this->repository->readAll($request);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException('Unable to read all data');
        }
    }

    public function readAllDoesntHaveUser($request, $userId)
    {
        try {
            $user = $this->userRepository->readOne($userId);

            if (!$user) {
                throw new InvalidArgumentException("User not found");
            }

            return $this->repository->readAllDoesntHaveUser($request, $user);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException('Unable to read all data');
        }
    }

    public function readAllPaginated($request)
    {
        try {
            return $this->repository->readAllPaginated($request);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to read all paginated data');
        }
    }

    public function update($request, $id)
    {
        $validator = Validator::make($request->all(), [
            "name" => "required|unique:packets,name,$id",
            "description" => "required",
            "price" => "required",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $fileName = str_shuffle("QWERTYUIOPASDFGHJKLZXCVBNM") . ".png";

            if ($request->image) {
                $request->path_image = uploadFileLocal("packets/image/$fileName", $request->image);
            }

            $update = $this->repository->update($request, $data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to update data");
        }

        DB::commit();
        return $update;
    }

    public function delete($id)
    {
        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $delete = $this->repository->delete($data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to delete data");
        }

        DB::commit();
        return $delete;
    }
}
