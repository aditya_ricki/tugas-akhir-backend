<?php

namespace App\Services;

use App\Jobs\ApproveScheduleJob;
use App\Jobs\NewScheduleJob;
use App\Jobs\ScheduleDoneJob;
use Throwable;
use InvalidArgumentException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use App\Repositories\Schedule\EloquentRepository;
use App\Repositories\PacketUser\EloquentRepository as PacketUserEloquentRepository;
use App\Repositories\Bill\EloquentRepository as BillEloquentRepository;
use App\Repositories\User\EloquentRepository as UserEloquentRepository;

class ScheduleService
{
    protected $repository, $packetUserRepository, $billRepository, $userRepository;

    public function __construct(EloquentRepository $repository, PacketUserEloquentRepository $packetUserRepository, BillEloquentRepository $billRepository, UserEloquentRepository $userRepository)
    {
        $this->repository = $repository;
        $this->packetUserRepository = $packetUserRepository;
        $this->billRepository = $billRepository;
        $this->userRepository = $userRepository;
    }

    public function create($request)
    {
        $validator = Validator::make($request->all(), [
            "date" => "required|date_format:Y-m-d",
            "packet_user_id" => "required",
            "description" => "required",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $packetUser = $this->packetUserRepository->readOne($request->packet_user_id);

            if (!$packetUser) {
                throw new InvalidArgumentException("Packet user not found");
            }

            if ($this->repository->check($packetUser)) {
                throw new InvalidArgumentException("There is still an unfinished schedule");
            }

            $request->type = "OTHER";
            $request->price = 0;
            $bill = $this->billRepository->create($request, $packetUser);

            if (!$bill) {
                throw new InvalidArgumentException("Bill not created");
            }

            $create = $this->repository->create($request, $bill);

            // notif email ke finance
            dispatch(new NewScheduleJob($create, $this->userRepository->readAllByRole(["F"])));
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }

        DB::commit();
        return $create;
    }

    public function readOne($id)
    {
        try {
            return $this->repository->readOne($id);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to read data by id");
        }
    }

    public function readAllByPacketUser($request, $packetUserId)
    {
        try {
            $packetUser = $this->packetUserRepository->readOne($packetUserId);

            if (!$packetUser) {
                throw new InvalidArgumentException("Packet user not found");
            }

            return $this->repository->readAllByPacketUser($request, $packetUser);
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            throw new InvalidArgumentException('Unable to read all data');
        }
    }

    public function readAllPaginatedByPacketUser($request, $packetUserId)
    {
        try {
            $packetUser = $this->packetUserRepository->readOne($packetUserId);

            if (!$packetUser) {
                throw new InvalidArgumentException("Packet user not found");
            }

            return $this->repository->readAllPaginatedByPacketUser($request, $packetUser);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException($th->getMessage());
        }
    }

    public function readAllPaginated($request)
    {
        try {
            return $this->repository->readAllPaginated($request);
        } catch (Throwable $th) {
            Log::info($th->getMessage());
            throw new InvalidArgumentException('Unable to read all paginated data');
        }
    }

    public function update($request, $id)
    {
        $validator = Validator::make($request->all(), [
            "date" => "required|date_format:Y-m-d",
            "description" => "required",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $update = $this->repository->update($request, $data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to update data");
        }

        DB::commit();
        return $update;
    }

    public function updateStatus($request, $id)
    {
        $validator = Validator::make($request->all(), [
            "status" => "required|in:0,1,2",
        ]);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);
            $scheduleStatus = $request->status;

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            if ($request->need_fee) {
                $bill = $this->billRepository->readOne($data->bill_id);

                if (!$bill) {
                    throw new InvalidArgumentException("Bill not found");
                }

                if (!$this->billRepository->update($request, $bill)) {
                    throw new InvalidArgumentException("Bill not updated");
                }
            }

            if ($request->activate) {
                $bill = $this->billRepository->readOne($data->bill_id);

                if (!$bill) {
                    throw new InvalidArgumentException("Bill not found");
                }

                $packetUser = $this->packetUserRepository->readOne($bill->packet_user_id);

                if (!$packetUser) {
                    throw new InvalidArgumentException("Packet user not found");
                }

                $request->status = 1;
                if (!$this->packetUserRepository->updateStatus($request, $packetUser)) {
                    throw new InvalidArgumentException("Packet user not updated");
                }
            }

            $update = $this->repository->updateStatus($scheduleStatus, $data);

            // notif email ke tech & customer
            if ($update && $scheduleStatus == 1) {
                dispatch(new ApproveScheduleJob($data, $this->userRepository->readAllByRole(["T"])));
            } else if ($update && $scheduleStatus == 2) {
                dispatch(new ScheduleDoneJob($data, $this->userRepository->readAllByRole(["F"])));
            }
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to update data");
        }

        DB::commit();
        return $update;
    }

    public function delete($id)
    {
        DB::beginTransaction();

        try {
            $data = $this->repository->readOne($id);

            if (!$data) {
                throw new InvalidArgumentException('Read one data return null');
            }

            $delete = $this->repository->delete($data);
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            throw new InvalidArgumentException("Unable to delete data");
        }

        DB::commit();
        return $delete;
    }
}
