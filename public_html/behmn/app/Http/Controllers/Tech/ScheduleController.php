<?php

namespace App\Http\Controllers\Tech;

use Throwable;
use Illuminate\Http\Request;
use App\Services\ScheduleService;
use App\Http\Controllers\Controller;

class ScheduleController extends Controller
{
    protected $request, $service, $result;

    function __construct(Request $request, ScheduleService $service)
    {
        $this->request = $request;
        $this->service = $service;
        $this->result = [
            'status' => 200,
            'message' => 'Success',
            'success' => true,
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getByPacketUser($packetUserId)
    {
        try {
            $this->result['data'] = $this->service->readAllPaginatedByPacketUser($this->request, $packetUserId);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        try {
            $this->result['data'] = $this->service->create($this->request);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $this->result['data'] = $this->service->readOne($id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        try {
            $this->result['data'] = $this->service->update($this->request, $id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateStatus($id)
    {
        try {
            $this->result['data'] = $this->service->updateStatus($this->request, $id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->result['data'] = $this->service->delete($id);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }
}
