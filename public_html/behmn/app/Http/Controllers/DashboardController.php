<?php

namespace App\Http\Controllers;

use Throwable;
use Illuminate\Http\Request;
use App\Services\DashboardService;

class DashboardController extends Controller
{
    protected $request, $service, $result;

    function __construct(Request $request, DashboardService $service)
    {
        $this->request = $request;
        $this->service = $service;
        $this->result = [
            'status' => 200,
            'message' => 'Success',
            'success' => true,
        ];
    }

    public function marketing()
    {
        try {
            $this->result['data'] = $this->service->getForMarketing($this->request);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function customer()
    {
        try {
            $this->result['data'] = $this->service->getForCustomer($this->request);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function tech()
    {
        try {
            $this->result['data'] = $this->service->getForTech($this->request);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }

    public function finance()
    {
        try {
            $this->result['data'] = $this->service->getForFinance($this->request);
        } catch (Throwable $th) {
            $this->result['status'] = 500;
            $this->result['message'] = $th->getMessage();
            $this->result['success'] = false;
        }

        return response()->json($this->result, $this->result['status']);
    }
}
