<?php

namespace App\Http\Controllers;

use Throwable;
use App\Models\Bill;
use Midtrans\Config;
use Midtrans\Notification;
use Illuminate\Http\Request;
use InvalidArgumentException;
use App\Jobs\SuccessPaymentJob;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MidtransController extends Controller
{
    public function notificationHandler()
    {
        Config::$serverKey = env('MIDTRANS_SERVER_KEY');
        Config::$isProduction = false;
        Config::$isSanitized = true;
        Config::$is3ds = true;

        try {
            $notif = new Notification();
        } catch (Throwable $th) {
            Log::error($th->getMessage());
            return false;
        }

        $notif = $notif->getResponse();
        $transaction = $notif->transaction_status;
        $type = $notif->payment_type;
        $order_id = $notif->order_id;
        $fraud = $notif->fraud_status ?? '';

        $bill = Bill::where('transaction_code', $order_id)->first();

        if (!$bill) {
            throw new InvalidArgumentException("Transaction not found");
        }

        if ($transaction == 'capture') {
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {
                if ($fraud == 'challenge') {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                    $message = "Transaction order_id: $order_id is challenged by FDS";
                    $status = 6;
                    $paymentUrl = '';
                } else {
                    // TODO set payment status in merchant's database to 'Success'
                    $message = "Transaction order_id: $order_id successfully captured using $type";
                    $status = 7;
                    $paymentUrl = $bill->payment_url;
                }
            }
        } else if ($transaction == 'settlement') {
            // TODO set payment status in merchant's database to 'Settlement'
            $message = "Transaction order_id: $order_id successfully transfered using $type";
            $status = 2;
            $paymentUrl = $bill->payment_url;
        } else if ($transaction == 'pending') {
            // TODO set payment status in merchant's database to 'Pending'
            $message = "Waiting customer to finish transaction order_id: $order_id using $type";
            $status = 1;
            $paymentUrl = $bill->payment_url;
        } else if ($transaction == 'deny') {
            // TODO set payment status in merchant's database to 'Denied'
            $message = "Payment using $type for transaction order_id: $order_id is denied.";
            $status = 3;
            $paymentUrl = '';
        } else if ($transaction == 'expire') {
            // TODO set payment status in merchant's database to 'expire'
            $message = "Payment using $type for transaction order_id: $order_id is expired.";
            $status = 4;
            $paymentUrl = '';
        } else if ($transaction == 'cancel') {
            // TODO set payment status in merchant's database to 'Denied'
            $message = "Payment using $type for transaction order_id: $order_id is canceled.";
            $status = 5;
            $paymentUrl = '';
        }

        DB::beginTransaction();
        try {
            $bill->update([
                'status' => $status,
                'paid_at' => $status == 2 || $status == 7 ? now() : null,
                'message' => $message,
                'payment_url' => $paymentUrl,
            ]);

            if (in_array($status, [2, 7])) {
                dispatch(new SuccessPaymentJob($bill, User::where('role', 'F')->get()));
            }
        } catch (Throwable $th) {
            DB::rollback();
            Log::error($th->getMessage());
            return false;
        }

        DB::commit();
        return true;
    }
}
