<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->id();
            $table->foreignId('packet_user_id')->constrained('packet_users')->onUpdate('cascade')->onDelete('cascade');
            $table->text('description')->nullable();
            $table->text('message')->nullable();
            $table->string('transaction_code', 20);
            $table->integer('price');
            $table->tinyInteger('status')->default(0);
            $table->text('payment_url')->nullable();
            $table->string('type')->default('MONTHLY');
            $table->string('method')->default('PAYMENT_GATEWAY');
            $table->datetime('paid_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}
