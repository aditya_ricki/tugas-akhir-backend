<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['F', 'M', 'T', 'C'];
        $names = ['Finance HMN', 'Marketing HMN', 'Tech HMN', 'Mela Sintiya Dewi'];
        $emails = ['finance@hmn.com', 'marketing@hmn.com', 'tech@hmn.com', 'melasd2205@gmail.com'];

        for ($i = 0; $i < 4; $i++) {
            User::create([
                'name' => $names[$i],
                'email' => $emails[$i],
                'email_verified_at' => date('Y-m-d H:i:s'),
                'password' => Hash::make('password'),
                'role' => $roles[$i],
                'phone' => '082' + str_shuffle(1234567890),
                'whatsapp' => '082' + str_shuffle(1234567890),
                'address' => 'Jl. Gunungkidul Yogyakarta',
            ]);
        }
    }
}
