<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
  <head>
    <title>
    </title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style type="text/css">
      #outlook a{padding: 0;}
      			.ReadMsgBody{width: 100%;}
      			.ExternalClass{width: 100%;}
      			.ExternalClass *{line-height: 100%;}
      			body{margin: 0; padding: 0; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%;}
      			table, td{border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;}
      			img{border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; -ms-interpolation-mode: bicubic;}
      			p{display: block; margin: 13px 0;}
    </style>
    <style type="text/css">
      @media only screen and (max-width:480px) {
      			  		@-ms-viewport {width: 320px;}
      			  		@viewport {	width: 320px; }
      				}
    </style>
    <style type="text/css">
      @media only screen and (max-width:480px) {

      			  table.full-width-mobile { width: 100% !important; }
      				td.full-width-mobile { width: auto !important; }

      }
      @media only screen and (min-width:480px) {
      .dys-column-per-100 {
      	width: 100.000000% !important;
      	max-width: 100.000000%;
      }
      }
    </style>
  </head>
  <body>
    <div>
      <div style='background:#eb721e;background-color:#eb721e;margin:0px auto;max-width:600px;'>
        <table align='center' border='0' cellpadding='0' cellspacing='0' role='presentation' style='background:#eb721e;background-color:#eb721e;width:100%;'>
          <tbody>
            <tr>
              <td style='direction:ltr;font-size:0px;padding:20px 0;text-align:center;vertical-align:top;'>
                <div class='dys-column-per-100 outlook-group-fix' style='direction:ltr;display:inline-block;font-size:13px;text-align:left;vertical-align:top;width:100%;'>
                  <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='vertical-align:top;' width='100%'>
                    <tr>
                      <td align='center' style='font-size:0px;padding:10px 25px;word-break:break-word;' colspan="2">
                        <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='border-collapse:collapse;border-spacing:0px;'>
                          <tbody>
                            <tr>
                              <td style='width:216px;'>
                                <img alt='' height='189' src='https://i1.wp.com/helesmedianetwork.com/wp-content/uploads/2021/05/LOGO.jpg?resize=300%2C300&ssl=1' style='border:none;display:block;font-size:13px;height:189px;outline:none;text-decoration:none;width:100%;' width='216' />
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td align='center' style='font-size:0px;padding:10px 25px;word-break:break-word;' colspan="2">
                        <div style="color:#FFFFFF;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:36px;line-height:1;text-align:center;">
                          NEW CUSTOMER
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style='font-size:0px;padding:10px 25px;'>
                        <div style="color:#FFFFFF;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:25px;line-height:1;">
                          Name
                        </div>
                      </td>
                      <td style='font-size:0px;padding:10px 25px;'>
                        <div style="color:#FFFFFF;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:25px;line-height:1;">
                          : {{ $packetUser->user->name }}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style='font-size:0px;padding:10px 25px;'>
                        <div style="color:#FFFFFF;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:25px;line-height:1;">
                          Phone / WA
                        </div>
                      </td>
                      <td style='font-size:0px;padding:10px 25px;'>
                        <div style="color:#FFFFFF;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:25px;line-height:1;">
                          : {{ $packetUser->user->phone }} / {{ $packetUser->user->whatsapp }}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td style='font-size:0px;padding:10px 25px;'>
                        <div style="color:#FFFFFF;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:25px;line-height:1;">
                          Email
                        </div>
                      </td>
                      <td style='font-size:0px;padding:10px 25px;'>
                        <div style="color:#FFFFFF;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:25px;line-height:1;">
                          : {{ $packetUser->user->phone }}
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td align='center' style='font-size:0px;padding:10px 25px;word-break:break-word;' vertical-align='middle' colspan="2">
                        <table border='0' cellpadding='0' cellspacing='0' role='presentation' style='border-collapse:separate;line-height:100%;width:200px;'>
                          <tr>
                            <td align='center' bgcolor='#178F8F' role='presentation' style='background-color:#FFFFFF;border:none;border-radius:4px;cursor:auto;padding:10px 25px;' valign='middle'>
                              <a href="https://www.google.com/maps/search/?api=1&query={{ $packetUser->latitude }},{{ $packetUser->longitude }}" style="background:#FFFFFF;color:#eb721e;font-family:'Droid Sans', 'Helvetica Neue', Arial, sans-serif;font-size:30px;font-weight:bold;line-height:30px;margin:0;text-decoration:none;text-transform:none;"
                              target="_blank">
                                Open Map
                              </a>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>
